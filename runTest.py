#!/usr/bin/env python
"""Runs MiniBrunel with a set of parameters"""

__author__ = "Sebastien Ponce"

import re
import os
import sys
import argparse
import subprocess
import tempfile
import itertools

def prepareInput(inputFileName, outputFileName, threads, events, useHive):
    '''create new input file from a base and options'''
    # write content to temp file
    fd, configFileName = tempfile.mkstemp(suffix=".py")
    outputFile = open(configFileName, 'w')
    outputFile.write('from Configurables import MiniBrunel\n')
    outputFile.write('mbrunel = MiniBrunel()\n')
    outputFile.write('mbrunel.EvtMax = %d\n' % events)
    if useHive:
        outputFile.write('mbrunel.ThreadPoolSize = %d\n' % threads)
        outputFile.write('mbrunel.EventSlots = %d\n' % (2*threads))
    outputFile.write('mbrunel.GECCut = 11500\n')
    outputFile.write('mbrunel.HLT1Only = True\n')
    outputFile.write('mbrunel.HLT1BestPerf = True\n')
    outputFile.write('mbrunel.EnableHive = %s\n' % useHive)
    outputFile.write('mbrunel.HLT1Fitter = True\n')
    outputFile.write('mbrunel.InputData = ["%s"]*%d\n' % (inputFileName, threads))
    outputFile.write('mbrunel.TimelineFile = "%s.csv"\n' % outputFileName)
    outputFile.write('mbrunel.RunFastForwardFitter = False\n')
    outputFile.write('mbrunel.EnableHLTEventLoopMgr = True\n')
    outputFile.write('mbrunel.IgnoreChecksum = True\n')
    outputFile.close()
    # return temp file name
    return configFileName

def runTest(inputFileName, baseOutputFileName, threads, firstevent, events, nbjobs, useHive, nbNumaNodes, measureMem):
    '''Run a test with the given options'''
    # start the different processes in parallel
    processes = []
    for jobNb in range(nbjobs):
        # create inputFile
        outputFileName = baseOutputFileName % jobNb
        configFileName = prepareInput(inputFileName, outputFileName, threads, threads*events, useHive)
        # open log file
        outputFile = open("%s.log" % outputFileName, 'w')
        # build command line
        cmdLine = ["gaudirun.py", configFileName]
        # memory measurement
        if measureMem:
            cmdLine = ["lbsmaps", "-o", "%s.xml" % outputFileName] + cmdLine
        # deal with numa if needed
        if nbNumaNodes > 1:
            node = jobNb % nbNumaNodes
            cmdLine = ["numactl", "-N", str(node), "--"] + cmdLine
        # run the test
        processes.append((subprocess.Popen(cmdLine, stdout=outputFile, stderr=subprocess.STDOUT), outputFile, configFileName))
    # wait for all processes and massage output
    for (process, outputFile, configFileName) in processes:
        process.wait()
        outputFile.close()
        # cleanup inputFile
        os.remove(configFileName)

def main():
    '''Main method : parses options and calls runTest'''
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-t', '--threads', type=str, default='1',
                        help='list of nb of threads per job to run the test for, comma separated')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-j', '--jobs', type=str, default='1',
                        help='list of nb of jobs to run the test for, comma separated')
    group.add_argument('-p', '--points', type=str,
                        help='explicit list of points (ie nb threads:nbjobs) to run the test for, comma separated')
    parser.add_argument('-m', '--maxthreads', type=int, default='-1',
                        help='maximum number of threads allowed. This allows to ignore some combinations of -t and -j that would lead to too many concurrent threads. Defaults to -1, that is disabled')
    parser.add_argument('-n', '--events', default=1000, type=int,
                        help='nb of events to process per thread')
    parser.add_argument('-e', '--firstevent', default=0, type=int,
                        help='nb of first event, default is 0')
    parser.add_argument('-r', '--runs', default=1, type=int,
                        help='nb of runs of the test, default is 1')
    parser.add_argument('-f', '--firstrun', default=0, type=int,
                        help='nb of first run, default is 0')
    parser.add_argument('-o', '--onlyhive', action='store_true',
                        help='whether to only run in hive mode, not running non hive cases')
    parser.add_argument('--numa', action='store_true',
                        help='whether to respect numa domains. If true, numactl is used to launch jobs in domains in a round-robin fashion')
    group.add_argument('--bestPoints', action='store_true',
                        help='let the tool run for the best combinations of threads and jobs for the node (checking number of cores and respecting numa settings)')
    group.add_argument('--memory', action='store_true',
                        help='activates memory measurements')
    parser.add_argument('inputFileName', type=str,
                        help='Input file name. Needs to have enough events for feeding 1 thread. Then it will be reused')
    args = parser.parse_args()

    # deal with numa config
    nbNumaNodes = 1
    if args.numa:
        try:
            output = subprocess.check_output(["numactl", "-show"])
            nodeline = [line for line in output.split('\n') if line.startswith('nodebind')][0]
            nbNumaNodes = len(nodeline.split()) - 1
        except:
            # numactl not existing
            print 'Warning : --numa given but numactl not found, ignoring --numa'

    # deal with list of points to use for the test
    if args.bestPoints:
        output = subprocess.check_output(["lscpu"])
        cpuline = [line for line in output.split('\n') if line.startswith('CPU(s):')][0]
        nbCores = int(cpuline.split()[-1])
        nbCoresPerNumaNode = nbCores / nbNumaNodes
        # fully threaded points : one job per numa node
        points = [(nbCoresPerNumaNode, nbNumaNodes), (nbCoresPerNumaNode+2, nbNumaNodes), (nbCoresPerNumaNode+4, nbNumaNodes)]
        # 2 jobs per numa node
        points += [(nbCoresPerNumaNode/2, 2*nbNumaNodes), (nbCoresPerNumaNode/2+1, 2*nbNumaNodes), (nbCoresPerNumaNode/2+2, 2*nbNumaNodes)]
    elif args.points:
        points = [map(int, item.split(':')) for item in args.points.split(',')]
    else:
        nbthreads = map(int,args.threads.split(','))
        nbjobs = map(int,args.jobs.split(','))
        points = itertools.product(nbthreads, nbjobs)
    print "Lists of configurations used (nbThreads, nbJobs) :", list(points)

    for nthreads, njobs in points:
        # check total number of threads
        if args.maxthreads > 0 and nthreads * njobs > args.maxthreads:
            continue
        # run the test the specified number of times
        for i in range(args.runs):
            outputFilebaseName = 'MiniBrunel-HTL1.%s.%dt.%dj.hive.%de.%d.%%d' % (os.environ['CMTCONFIG'], nthreads, njobs, args.events, args.firstrun+i)
            runTest(args.inputFileName, outputFilebaseName, nthreads, args.firstevent, args.events, njobs, True, nbNumaNodes, args.memory)
        # in case of single thread, maybe also run non hive mode
        if nthreads == 1 and not args.onlyhive:
            for i in range(args.runs):
                outputFilebaseName = 'MiniBrunel-HTL1.%s.1t.%dj.nohive.%de.%d.%%d' % (os.environ['CMTCONFIG'], njobs, args.events, args.firstrun+i)
                runTest(args.inputFileName, outputFilebaseName, nthreads, args.firstevent, args.events, njobs, False, nbNumaNodes, args.memory)

if __name__ == '__main__':
    sys.exit(main())

# Nice list of points for getting quickly something visible and nice plots in the long term
# 1:1,1:16,1:32,1:48,1:64,2:1,2:8,2:16,2:24,2:32,4:1,4:4,4:8,4:12,4:16,8:1,8:2,8:4,8:6,8:8,12:1,12:2,12:4,12:5,16:1,16:2,16:3,16:4

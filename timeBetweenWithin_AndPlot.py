#usage (i)python parse_timelineInformation.py path/to/timeline.csv
from __future__ import print_function
import numpy as np
from sys import argv
import pandas as pd
import matplotlib.pyplot as plt

#parse from csv file
full_data = pd.read_csv(argv[1], delimiter= ' ')

data_split_names = full_data.groupby('algorithm')
data_split_thread = full_data.groupby('thread')

total_sum_within_algs = 0 #sum of time taken within all algorithms
total_sum_between_algs = 0 #sum of time taken between each two adjacent algorithms
total_sum_between_events = 0 #startup time of an event?

for datatup in data_split_thread:
    data = datatup[1]
    sum_within_algs = 0
    sum_between_algs = 0

    for i in range(len(data)):
        sum_within_algs += data.iloc[i]['end'] - data.iloc[i]['#start']
    for i in range(len(data)-1):
        sum_between_algs += data.iloc[i+1]['#start'] - data.iloc[i]['end']
        if data.iloc[i+1]['event'] != data.iloc[i]['event']:
            total_sum_between_events += data.iloc[i+1]['#start'] - data.iloc[i]['end']

    total_sum_within_algs += sum_within_algs
    total_sum_between_algs += sum_between_algs



print("\ntotal sum of time between algs: {}".format(total_sum_between_algs))
print("startup time (time between events) : {}".format(total_sum_between_events))
print("This is about {:.1f}% of the total in-between time (between event / between algs)".format(total_sum_between_events*100./total_sum_between_algs))
print("--------------------------------------------------------")
print("total sum of time within algs: {}".format(total_sum_within_algs))
print("about {:.1f}% of the total time is used between algs (between / (within+between))".format(total_sum_between_algs*100./(total_sum_within_algs+total_sum_between_algs)))
print("--------------------------------------------------------")


algtimes = dict()
for datatup in data_split_names:
    data = datatup[1]
    sum_alg_time = 0
    for i in range(len(data)):
        sum_alg_time += data.iloc[i]['end'] - data.iloc[i]['#start']

    algtimes[datatup[0]] = sum_alg_time #data[0,2] = algorithm name


#final summary
for k,v in algtimes.items():
    print( "total time of {}: {}".format(k,v))

algtimes_sorted = dict(sorted(algtimes.items(), key=lambda x:x[1]))

plt.barh(range(len(algtimes_sorted)), list(algtimes_sorted.values()), align='center')
plt.yticks(range(len(algtimes_sorted)), list(algtimes_sorted.keys()))

if len(argv) == 3:
    plt.savefig(argv[2])

plt.show()

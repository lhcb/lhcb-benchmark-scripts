#!/usr/bin/env python
"""Extracts throughputs from a set of log/csv files resulting from running a MiniBrunel benchmark"""

__author__ = "Sebastien Ponce"

import sys, os, csv
from multiprocessing import Pool

sequences = ["BrunelSequencer", "PhysicsSeq", "Reco", "RecoDecodingSeq", "RecoTrFastSeq"]

def extractMem(fileName):
    f = open(fileName, 'r')
    maxi = 0
    for l in f.readlines():
        if not l.startswith('<process'): continue
        rss = int(l.split()[3][5:-1])/1000.0
        if rss > maxi:
            maxi = rss
    f.close()
    return maxi

def extractEvents(fileName):
    events = {}
    lastEvt = 0
    with open(fileName, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        titles = reader.next()
        for s, e, a, t, sl, ne in reader:
            ne = int(ne)
            s = int(s)
            e = int(e)
            if ne in events:
                ps, pe = events[ne]
                events[ne] = (min(s, ps), max(e, pe))
            else:
                events[ne] = (s, e)
            if ne > lastEvt:
                lastEvt = ne
    return lastEvt, events

def extractTimes(allEvents):
    '''find out latest event number 10 and earliest last event,
       then drop all events outside these bounds and count remaining
       ones plus measure the time spent'''
    startTime = 0
    endTime = 100000000000000000000000000
    for lastEvt, events in allEvents:
        startTime = max(startTime, events[10][0])
        endTime = min(endTime, events[lastEvt][1])
    nbEvts = 0
    for lastEvt, events in allEvents:
        for ne in events:
            if ne < 10: continue
            if events[ne][0] < startTime or events[ne][1] > endTime: continue
            nbEvts += 1
    return nbEvts, startTime, endTime
    
resTime = {}

# list files per type and key (for csv ones)
print 'Listing Files'
xmlFiles = []
csvFiles = {}
prevNbEvents = -1
nbRuns = 0
for d in sys.argv[1:]:
    for fileName in os.listdir(d):
        if fileName[0:10] == 'MiniBrunel':
            if fileName[-3:] == 'xml' :
                xmlFiles.append(fileName)
            elif fileName[-3:] == 'csv':
                print 'using file %s/%s            \r' % (d, fileName),
                sys.stdout.flush()
                base, cmtconfig, threads, jobs, isHive, events, runNb, job, ext = fileName.split('.')
                run = int(runNb)
                if run > nbRuns:
                    nbRuns = run+1
                nbEvents = int(events[:-1])
                if prevNbEvents < 0:
                    prevNbEvents = nbEvents
                else:
                    if prevNbEvents != nbEvents:
                        print 'Inconsistent nb of events in the different files. This is not supported. Giving up'
                        sys.exit(1)
                key = (int(threads[:-1]), int(jobs[:-1]), isHive=='hive')
                if cmtconfig not in csvFiles:
                    csvFiles[cmtconfig] = {}
                if key not in csvFiles[cmtconfig]:
                    csvFiles[cmtconfig][key] = {}
                if run not in csvFiles[cmtconfig][key]:
                    csvFiles[cmtconfig][key][run] = []
                csvFiles[cmtconfig][key][run].append('%s/%s' % (d, fileName))
print

# dealing with csv files
resTime = {}
print 'Extracting timing data'
for cmtconfig in csvFiles:
    resTime[cmtconfig] = {}
    for key in csvFiles[cmtconfig]:
        for run in csvFiles[cmtconfig][key]:
            startTime = 0
            endtime = 100000000000000000000000000
            allEvents = []
            if len(csvFiles[cmtconfig][key][run]) != key[1]:
                print 'MISSING FILE for %s, nbthread=%d, nbjobs=%d : %d out of %d present' % (cmtconfig, key[0], key[1], len(csvFiles[cmtconfig][key][run]), key[1])
            # parse files in parallel
            pool = Pool()
            results = []
            for fileName in csvFiles[cmtconfig][key][run]:
                print 'using file %s              \r' % fileName,
                sys.stdout.flush()
                results.append(pool.apply_async(extractEvents, ["%s/%s" % (d, fileName)]))
            for result in results:
                allEvents.append(result.get())
            nbevts, startTime, endTime = extractTimes(allEvents)
            if endTime < startTime or nbevts == 0:
                sys.stderr.write('\nWarning, unable to get consistent data for run %d with %d threads and %d jobs\n' % (run, key[0], key[1]))
            else:
                if key in resTime[cmtconfig]:
                    curNbEvts, curTime = resTime[cmtconfig][key]
                else:
                    curNbEvts, curTime = 0, 0
                resTime[cmtconfig][key] = (nbevts + curNbEvts, (endTime-startTime)/1000000000.0 + curTime)
print

# handle xml files
resMem = {}
print 'Extracting memory data'
for fileName in xmlFiles:
    print 'using file %s/%s \r' % (d, fileName),
    sys.stdout.flush()
    value = extractMem("%s/%s" % (d, fileName))
    base, cmtconfig, threads, jobs, isHive, events, runNb, job, ext = fileName.split('.')
    run = int(runNb)
    key = (int(threads[:-1]), int(jobs[:-1]), isHive=='hive')
    if cmtconfig not in resMem:
        resMem[cmtconfig] = {}
    if key not in resMem[cmtconfig]:
        resMem[cmtconfig][key] = [0]*(nbRuns+1)
    resMem[cmtconfig][key][run] += value # mem taken is sum of different jobs
print

f = open('extractedData', 'w')
f.write(str((resTime, resMem)))
f.close()

import sys
import numpy
import matplotlib.pyplot as plt
import matplotlib.cm as cm

def extractData(fileName, skipEvts=10):
    f = open(fileName, 'r')
    res = {}
    evtset = set([])
    for l in f.readlines()[1:]:
        s, e, a, t, sl, ne = l.split()  #start end algorithm thread slot event
        if a.find('Seq') >= 0 or a == 'Reco':
            continue
        evtset.add(ne)
        s = int(s)
        e = int(e)
        if a not in res:
            res[a] = 0
        res[a] += e-s
    f.close()
    return len(evtset), res

nbevts, data = extractData(sys.argv[1])
print nbevts, data

# cleanup data, by merging everything below 2%
cdata = {}
s = sum(data.values())
other = 0
for alg in data:
    if data[alg] < s*0.025:
        other += data[alg]
    else:
        cdata[alg] = data[alg]
cdata['other'] = other

# compute time spent per event
throughput = 1000000000.0*nbevts/s

# Setup colors
colors = cm.rainbow(numpy.linspace(0., 1., len(cdata.keys())))

plt.axes(aspect=1)
plt.pie(cdata.values(), [0.02] * len(cdata.keys()), cdata.keys(), autopct='%1.1f%%', shadow=True, startangle=90, colors=colors)
#plt.title('Throughput : %.1f evts/s/thread' % throughput)
#plt.title('Time Usage in MiniBrunel')
plt.show()

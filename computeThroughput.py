#!/usr/bin/env python
"""Compute reference throughput from the extracted data of a MiniBrunel benchmark"""

__author__ = "Sebastien Ponce"

import sys

times, mems = eval(open(sys.argv[1]).read())

# Extract data from input
if len(times.keys()) > 1:
    print "Not supporting mixed CMTCONFIG, giving up"
    sys.exit(-1)
times = times[times.keys()[0]]

# go though full list of points and extract throughput for each
scores = []
for nt, nj, useHive in times:
    nbEvents, duration = times[(nt,nj,useHive)]
    throughput = nbEvents/duration
    scores.append(throughput)

# find best scores and average them
sscores = sorted(scores)[::-1][:6]
print sum(sscores)/len(sscores)

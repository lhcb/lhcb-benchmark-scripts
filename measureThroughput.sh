#!/bin/sh

# create a temporary directory for benchmark logs
rm -rf results
mkdir results
cd results

# run effectively the benchmark
runTest.py --bestPoints -n 10000 --numa $1

# extract throughput from the logs
extract.py . >& /dev/null
cd ..

# compute final number
throughput=$(computeThroughput.py results/extractedData 2>&1)

# tar and zip the logs
tar cjf results.tbz results

# cleanup
rm -rf results

# spit out the output
echo "Throughput : $throughput"

import sys, os, numpy

times, mems = eval(open(sys.argv[1]).read())

import matplotlib.pyplot as plt
plt.xlabel('Nb cores/threads')
plt.ylabel('Max resident memory usage (MB)')
plt.title('Memory usage on multi-cores')

for cmtconfig in mems:
    nbJobsSet = set([nj for (nt,nj,ishive) in mems[cmtconfig].keys() if ishive])
    for nbJobs in sorted(list(nbJobsSet)):
        keys = sorted([(nt,nj,ishive) for (nt,nj,ishive) in mems[cmtconfig].keys() if nj == nbJobs and ishive])
        plt.plot([nt*nj for (nt,nj,ishive) in keys], [mems[cmtconfig][(nt,nj,ishive)] for (nt,nj,ishive) in keys], marker='o', label='%d jobs' % (nbJobs))

plt.legend(loc=4)
plt.show()

This repository provides a set of scripts to ease benchmarking of Minibrunel.
You will find there 8 scripts and here is a short description of each of them

The first one to use if runTest.py, I let you guess what it does.
Check the help, but main things you want to know are :
  * MiniBrunel config is hardcoded in there, you have to change it by hand at the top in prepareInput
  * default config will run HLT1 for 1000 events per thread/job, with 1 thread, 1 job, in hive mode and without the fit
  - -t allows to give the number of threads you want to use, for example -t 1,2,4 will run 3 times MiniBrunel with 1, 2 and 3 threads
  - -j is similar for number of jobs
  - all combinations of -t and -j are ran : -j 1,2 -t 1,2 will run 4 tests. you can limit for global concurrency with -m
  - -p allows to explicit list of points (ie nb threads:nbjobs) to run the test for, comma separated. Thus -j and -t ar then ignored
  - --bestPoints let the tool run for the 6 best combinations of threads and jobs for the node (checking number of cores). thus -p, -j and -t will be ignored
  - -n allows to change number of event per thread/job
  - -r allows to run several time each config, by default it's only once
  - -o allows to run only in hive mode. By default for nbThreads=1, both hive and non-hive versions are ran
  - --numa allows to respect numa nodes. Jobs will be launched using numactl on the different numa nodes in a round-robin fashion. So you probably want to have the number of job being a multiple of the number of numa nodes
  - it takes a single argument : the input file name
  - finally it internally uses lbsmaps, so you need to have this script in your PATH

Take care when you run to use as input a file in RAMFS if you do not want to be slowed down by IO.

The output of runTest.py is a set of .log, .csv and .xml files. One of each per test ran and per job of the test. They will appear in the current directory and are named after the parameters of the test.
Put them in a given directory before you use the other scripts.

extract.py is the next one you may use : you give it a directory full of log, csv and xml files and it parses all of them to extract the key numbers into a file called extractedData (yes, hardcoded name, to be changed).
It only contains a python tuple of dictionaries. That's the one you need for all subsequent plots.

computeTroughput.py computes a single throughput number from the extractedData file. This number is the average of the 6 best throughputs achieved.

plotMem.py and plotSpeedup.py each take as input this extractedData and plot stuff mem/speedup with respect to parallelism level (nb threads*nb jobs)

plotAlgoUsage.py and hivetimeline.py are different as they take in input a csv file of one single test and they plot what happened during that test.
  - plotAlgoUsage.py plots a pie chart of the relative time spent in the different algorithms during the test
  - hivetimeline.py plots the timeline of the job, with the scheduling of each algo and event on each core.
    Take care that this one will not work so well with many events. You probably want to have no more than a few 10s per thread. One trick is to just take the head -1000 of the csv file and see the start of the job

measureThroughput.sh wraps up all the rest to compute the reference throughput of a machine by running test, extracting throughputs and evraging them. It is a light wrapper around runTest, extract and computeThroughput